<samp>
  <div align ="center" >
    <img height ="60em" src="https://media.giphy.com/media/Qo2dupDib32rkTY4hX/giphy.gif"/>
  </div>

  <div align="center">
 <b align="">Sobre mim:</b>
 <p align="">😃 Meu nome é Pedro, tenho 24 anos e me aventuro no mundo do código há 3.</p>
 <p align="">Amante da arte e apaixonado por ensinar e aprender, estou sempre em busca de aprimorar meus conhecimentos.</p>
 <p align="">🌱 Atualmente estudando desenvolvimento Mobile e Backend utilizando os frameworks React Native e Django.</p>    
 <p align=""> Confira um pouco do meu trabalho neste <a href="https://pdr-tuche.github.io/portifolio/" target="_blank">portfólio</a> 👈</p>
  </div>
  <br>
  
  <div align= "center">
  <p><b>clique no card abaixo para entrar em contato</b></p>
     <a href="https://beacons.ai/pdrtuche" target="_blank"><img src="https://lanyard.cnrad.dev/api/259092600978407435?idleMessage=Nothing%20at%20the%20moment%20...%20Maybe%20i'm%20sleeping%20zzz&animated=true&theme=dark&borderRadius=30px&hideBadges=false&hideDiscrim=true&bg=1a1b27&hideTimestamp=true"/></a>
    <br>
    <br>
   <p>📲 Pode me mandar mensagem no <a href ="https://t.me/pdrTuche">Telegram</a> vou gostar de conversar sobre tecnologia com você ^^</p>
  </div>
  
</samp>
<br>

<details>
  <summary> <b>📊 GitHub Stats </b><i >(clique para expandir !!)</i> </summary>
  <br>
<div align="center">
  <a href = "https://github.com/pdr0nvs">
        <img height="180em" src="https://github-readme-stats.vercel.app/api?username=pdr-tuche&show_icons=true&line_height=20&theme=tokyonight&hide_border=true&hide_rank=true&include_all_commits=true&count_private=true&locale=pt-br">
        <img height="180em" src="https://github-readme-streak-stats.herokuapp.com/?user=pdr-tuche&theme=tokyonight&hide_border=true&locale=pt-br&fire=FF6347">
        <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=pdr-tuche&langs_count=6&layout=compact&line_height=30&hide=Tcl&locale=pt-br&theme=tokyonight&hide_border=true"> 
    </a>
</div>
  <samp>
  <div align="center">
    <p><b>Profile Views:</b></p>
  <img height ="20px" src="https://profile-counter.glitch.me/{pdr-tuche}/count.svg"> 
 <!--<img src="https://komarev.com/ghpvc/?username=pdr-tuche&style=flat-square&color=1a1b27&label=profile+views"> -->
  </div>
</samp>
</details>
